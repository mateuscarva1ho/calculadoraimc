﻿using System;
using System.Collections.Generic;
using System.Text;

namespace calculadora
{
    class calcular
    {
        public double Num1 { get; set; }
        public double Num2 { get; set; }
        public string Operacao { get; set; }

        public double ExecutarCalculo()
        {
            if (this.Operacao == "somar")
            {
                return this.Num1 + this.Num2;
            }
            else if (this.Operacao == "subtrair")
            {
                return this.Num1 - this.Num2;
            }
            else if (this.Operacao == "multiplicacao")
            {
                return this.Num1 * this.Num2;
            }
            else if (this.Operacao == "divisao")
            {
                return this.Num1 / this.Num2;
            }

            return 0;
        }
    }
}
