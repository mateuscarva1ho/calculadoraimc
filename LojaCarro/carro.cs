﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LojaCarro
{
    class carro
    {
        public int Ano { get; set; }
        public int NumeroDonos { get; set; }
        public int Marca { get; set; }
        public int modelo { get; set; }
        public int Km { get; set; }
        public int Preco { get; set; }
        public int Cor { get; set; }

        public double ValorParcela(int parcela)
        {
            Console.WriteLine("Calculando parcela");
            if (parcela > 24)
                return 0;
            double Valorparcela = this.Preco / parcela;
            return Valorparcela;

        }
        public string TipoCarro()
        {
            Console.WriteLine("Verificando tipo do carro");
            int idadeCarro = int.Parse(DateTime.Now.Year.ToString()) - this.Ano;
            double kmPorAno = this.Km / idadeCarro;


            if (this.Km == 0)
            {
                return "zero";
            }
            else if (idadeCarro <= 3)
            {
                if (this.NumeroDonos == 1) ;
                {
                    if(kmPorAno > 20000)
                    {
                        return "seminovo";
                    }
                    else
                    {
                        return "usado";

                    }
                }
            }
            else
            {
                return "usado";
            }
        }

    }
}
